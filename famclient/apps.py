from django.apps import AppConfig


class FamclientConfig(AppConfig):
    name = 'famclient'
