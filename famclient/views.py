from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from django.contrib import messages
import requests, json
import re
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password

# Create your views here.

client_id = 'YEDIJYz5cTRYtvze99LaGMfbT1ChaOCZYZpCg4Z0'
client_secret = 'JYg144lDZ535Ux650dPd2tAwnLrbDhRaNqS44Eos4YgbBnbnU1fEaVm0XnwwnbBWIWCCpsB2cun4eSbOcy49JrtOABVGgMYkKqxjWjByp5lA8UwI3xvdozOtSL1KeGoi'
client_user = 'rafamus'
client_password = '15264859'


def index(request):
    token = str(request.session.get('accesstoken'))
    isLoggedIn = request.session.get('isLoggedIn')
    loggedInUser = request.session.get('loggedInUser')
    if token is not None and isLoggedIn:
        family = requests.get('http://localhost:8000/api/family/'+str(loggedInUser["id"]), headers={'Authorization': 'Bearer {}'.format(token)})
        if family.content:
            familyjson = json.loads(family.content)
            familylistowner_id = familyjson["familylistowner_id"]
            relations = familyjson["relations"].split(",")
            firstnames = familyjson["firstnames"].split(",")
            lastnames = familyjson["lastnames"].split(",")
            birthdays = familyjson["birthdays"].split(",")
            vitalstatuses = familyjson["vitalstatuses"].split(",")
            familyarray = []
            for i in range(len(relations)):
                familyarray.append({"familylistowner_id":familylistowner_id,"relation":relations[i], "firstname":firstnames[i], "lastname":lastnames[i], "birthday":birthdays[i], "vitalstatus":vitalstatuses[i]})
            return render(request, 'index.html', {'family': familyarray, 'user':loggedInUser, 'isLoggedIn':isLoggedIn})
    else:
        return redirect('/signin')

def signup(request):
    isLoggedIn = request.session.get('isLoggedIn')
    if isLoggedIn:
        return redirect('/')
    if request.method == 'GET':
        return render(request, 'signup.html')
    else:
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        username = request.POST['username']
        birthday = request.POST['birthday']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        post_data = {'client_id': client_id,
        'client_secret': client_secret,
        'username': client_user,
        'password': client_password,
        'grant_type': 'password'}
        response = requests.post('http://localhost:8000/o/token/', data=post_data)
        if response.content:
            jsonres = json.loads(response.content)
            accesstoken = jsonres["access_token"]
            request.session['accesstoken'] = str(accesstoken)
            getusers = requests.get('http://localhost:8000/api/users/', headers={'Authorization': 'Bearer {}'.format(str(accesstoken))})
            if getusers.content:
                users = json.loads(getusers.content)
                if password1 == password2:
                    if ([x for x in users if x['username'] == username]):
                        messages.info(request, 'Username is already used.')
                        return redirect('signup')
                    elif ([x for x in users if x['email'] == email]):
                        messages.info(request, 'Email is already used.')
                        return redirect('signup')
                    else:
                        register_userdata = {
                            'username': username,
                            'first_name': firstname,
                            'last_name': lastname,
                            'email': email,
                            'password': password1
                        }
                        registeruser = requests.post('http://localhost:8000/api/users/', data=register_userdata, headers={'Authorization': 'Bearer {}'.format(str(accesstoken))})
                        if registeruser.content:
                            newuser = json.loads(registeruser.content)
                            request.session['isLoggedIn'] = True
                            request.session['loggedInUser'] = newuser
                            post_datafamily = {"familylistowner_id":newuser["id"],"relations":'Me',"firstnames":firstname,"lastnames":lastname,"birthdays":birthday,"vitalstatuses":"Alive"}
                            addusertofamily = requests.post('http://localhost:8000/api/family/', data=post_datafamily, headers={'Authorization': 'Bearer {}'.format(str(accesstoken))})
                            familyarray = []
                            if addusertofamily.content:
                                familyjson = json.loads(addusertofamily.content)
                                relations = familyjson["relations"]
                                firstnames = familyjson["firstnames"]
                                lastnames = familyjson["lastnames"]
                                birthdays = familyjson["birthdays"]
                                vitalstatuses = familyjson["vitalstatuses"]
                                familyarray.append({"relation":relations, "firstname":firstnames, "lastname":lastnames, "birthday":birthdays, "vitalstatus":vitalstatuses})
                                return render(request, 'index.html', {'family': familyarray, 'accesstoken':accesstoken, 'user': newuser, 'isLoggedIn':request.session.get('isLoggedIn')})
                else:
                    messages.info(request, 'Passwords do not match.')
                    return redirect('signup')

def signin(request):
    isLoggedIn = request.session.get('isLoggedIn')

    if isLoggedIn:
        return redirect('/')
    if request.method == 'GET':
        return render(request, 'signin.html')
    else:
        username = request.POST['username']
        password = request.POST['password']
        post_data = {'client_id': client_id,
        'client_secret': client_secret,
        'username': username,
        'password': password,
        'grant_type': 'password'}
        response = requests.post('http://localhost:8000/o/token/', data=post_data)
        jsonres = json.loads(response.content)
        if "access_token" in jsonres:
            accesstoken = jsonres["access_token"]
            request.session['accesstoken'] = str(accesstoken)
            login = requests.post('http://localhost:8000/api/login/', data={"username":username, "password":password}, headers={'Authorization': 'Bearer {}'.format(str(accesstoken))})
            if login.content:
                loggedInUser = json.loads(login.content)
                request.session['isLoggedIn'] = True
                request.session['loggedInUser'] = loggedInUser
                userid = loggedInUser["id"]
                family = requests.get('http://localhost:8000/api/family/'+str(userid), headers={'Authorization': 'Bearer {}'.format(str(accesstoken))})
                if family.content:
                    familyjson = json.loads(family.content)
                    relations = familyjson["relations"].split(",")
                    firstnames = familyjson["firstnames"].split(",")
                    lastnames = familyjson["lastnames"].split(",")
                    birthdays = familyjson["birthdays"].split(",")
                    vitalstatuses = familyjson["vitalstatuses"].split(",")
                    familyarray = []
                    for i in range(len(relations)):
                        familyarray.append({"relation":relations[i], "firstname":firstnames[i], "lastname":lastnames[i], "birthday":birthdays[i], "vitalstatus":vitalstatuses[i]})
                    return render(request, 'index.html', {'family': familyarray, 'accesstoken':accesstoken, 'user': loggedInUser, 'isLoggedIn':request.session.get('isLoggedIn')})
            else:
                messages.info(request, 'Invalid credentials')
                return redirect('signin')
        else:
            messages.info(request, 'Invalid credentials')
            return redirect('signin')

def signout(request):
    isLoggedIn = request.session.get('isLoggedIn')
    if isLoggedIn:
        token = str(request.session.get('accesstoken'))
        post_data = {'token': token,
        'client_id': client_id,
        'client_secret': client_secret
        }
        response = requests.post('http://localhost:8000/o/revoke_token/', data=post_data)
        if not response.content:
            request.session['isLoggedIn'] = False
            request.session['loggedInUser'] = None
            request.session['accesstoken'] = ""
            return redirect('/signin')
    else:
        return redirect('/')

def add(request):
    token = str(request.session.get('accesstoken'))
    isLoggedIn = request.session.get('isLoggedIn')
    loggedInUser = request.session.get('loggedInUser')
    if isLoggedIn:
        if request.method == 'POST':
            relation = request.POST['relation']
            firstname = request.POST['firstname']
            lastname = request.POST['lastname']
            birthday = request.POST['birthday']
            vitalstatus = request.POST['vitalstatus']

            family = requests.get('http://localhost:8000/api/family/'+str(loggedInUser["id"]), headers={'Authorization': 'Bearer {}'.format(token)})
            if family.content:
                familyjson = json.loads(family.content)
                familylistowner_id = str(familyjson["familylistowner_id"])
                relations = str(familyjson["relations"]) + "," + str(relation)
                firstnames = str(familyjson["firstnames"]) + "," + str(firstname)
                lastnames = str(familyjson["lastnames"]) + "," + str(lastname)
                birthdays = str(familyjson["birthdays"]) + "," + str(birthday)
                vitalstatuses = str(familyjson["vitalstatuses"]) + "," + str(vitalstatus)
                familyput = requests.put('http://localhost:8000/api/family/'+str(loggedInUser["id"]), data = {"familylistowner_id":familylistowner_id,"relations":relations,"firstnames":firstnames,"lastnames":lastnames,"birthdays":birthdays,"vitalstatuses":vitalstatuses}, headers={'Authorization': 'Bearer {}'.format(str(token))})
                if familyput.content:
                    return redirect('/')
    else:
        return redirect('/signin')

def edit(request, index):
    token = str(request.session.get('accesstoken'))
    isLoggedIn = request.session.get('isLoggedIn')
    loggedInUser = request.session.get('loggedInUser')
    if isLoggedIn:
        if request.method == 'POST':
            relation = request.POST['relation']
            firstname = request.POST['firstname']
            lastname = request.POST['lastname']
            birthday = request.POST['birthday']
            vitalstatus = request.POST['vitalstatus']
            family = requests.get('http://localhost:8000/api/family/'+str(loggedInUser["id"]), headers={'Authorization': 'Bearer {}'.format(token)})
            if family.content:
                familyjson = json.loads(family.content)
                familylistowner_id = familyjson["familylistowner_id"]
                relations = familyjson["relations"].split(",")
                firstnames = familyjson["firstnames"].split(",")
                lastnames = familyjson["lastnames"].split(",")
                birthdays = familyjson["birthdays"].split(",")
                vitalstatuses = familyjson["vitalstatuses"].split(",")
                familyarray = []
                for i in range(len(relations)):
                    familyarray.append({"relation":relations[i], "firstname":firstnames[i], "lastname":lastnames[i], "birthday":birthdays[i], "vitalstatus":vitalstatuses[i]})
                familyarray[index]["relation"] = relation
                familyarray[index]["firstname"] = firstname
                familyarray[index]["lastname"] = lastname
                familyarray[index]["birthday"] = birthday
                familyarray[index]["vitalstatus"] = vitalstatus

                relationsupdated = ""
                firstnamesupdated = ""
                lastnamesupdated = ""
                birthdaysupdated = ""
                vitalstatusesupdated = ""

                for i in range(len(familyarray)):
                    relationsupdated = relationsupdated + "," + str(familyarray[i]["relation"])
                    firstnamesupdated = firstnamesupdated + "," + str(familyarray[i]["firstname"])
                    lastnamesupdated = lastnamesupdated + "," + str(familyarray[i]["lastname"])
                    birthdaysupdated = birthdaysupdated + "," + str(familyarray[i]["birthday"])
                    vitalstatusesupdated = vitalstatusesupdated + "," + str(familyarray[i]["vitalstatus"])
                relationsupdated = relationsupdated[1:]
                firstnamesupdated = firstnamesupdated[1:]
                lastnamesupdated = lastnamesupdated[1:]
                birthdaysupdated = birthdaysupdated[1:]
                vitalstatusesupdated = vitalstatusesupdated[1:]
                familyput = requests.put('http://localhost:8000/api/family/'+str(loggedInUser["id"]), data = {"familylistowner_id":familylistowner_id,"relations":relationsupdated,"firstnames":firstnamesupdated,"lastnames":lastnamesupdated,"birthdays":birthdaysupdated,"vitalstatuses":vitalstatusesupdated}, headers={'Authorization': 'Bearer {}'.format(str(token))})
                if index == 0:
                    updateuser = requests.put('http://localhost:8000/api/users/'+str(loggedInUser["id"])+'/', data = {"id":loggedInUser["id"],"first_name":firstname, "last_name":lastname}, headers={'Authorization': 'Bearer {}'.format(str(token))})
                    if updateuser.content:
                        updateduserjson = json.loads(updateuser.content)    
                        request.session['loggedInUser'] = updateduserjson
                if familyput.content:
                    return redirect('/')
        else:
            family = requests.get('http://localhost:8000/api/family/'+str(loggedInUser["id"]), headers={'Authorization': 'Bearer {}'.format(token)})
            if family.content:
                familyjson = json.loads(family.content)
                familylistowner_id = familyjson["familylistowner_id"]
                relations = familyjson["relations"].split(",")
                firstnames = familyjson["firstnames"].split(",")
                lastnames = familyjson["lastnames"].split(",")
                birthdays = familyjson["birthdays"].split(",")
                vitalstatuses = familyjson["vitalstatuses"].split(",")
                familyarray = []
                for i in range(len(relations)):
                    familyarray.append({"relation":relations[i], "firstname":firstnames[i], "lastname":lastnames[i], "birthday":birthdays[i], "vitalstatus":vitalstatuses[i]})
                return render(request, 'edit.html', {'family': familyarray[index], 'user':loggedInUser, 'isLoggedIn':isLoggedIn})
    else:
        return redirect('/signin')

def delete(request, index):
    token = str(request.session.get('accesstoken'))
    isLoggedIn = request.session.get('isLoggedIn')
    if isLoggedIn:
        loggedInUser = request.session.get('loggedInUser')
        family = requests.get('http://localhost:8000/api/family/'+str(loggedInUser["id"]), headers={'Authorization': 'Bearer {}'.format(token)})
        if family.content:
            familyjson = json.loads(family.content)
            familylistowner_id = familyjson["familylistowner_id"]
            relations = familyjson["relations"].split(",")
            firstnames = familyjson["firstnames"].split(",")
            lastnames = familyjson["lastnames"].split(",")
            birthdays = familyjson["birthdays"].split(",")
            vitalstatuses = familyjson["vitalstatuses"].split(",")
            familyarray = []
            for i in range(len(relations)):
                familyarray.append({"relation":relations[i], "firstname":firstnames[i], "lastname":lastnames[i], "birthday":birthdays[i], "vitalstatus":vitalstatuses[i]})
            
            del familyarray[index]

            relationsupdated = ""
            firstnamesupdated = ""
            lastnamesupdated = ""
            birthdaysupdated = ""
            vitalstatusesupdated = ""

            for i in range(len(familyarray)):
                relationsupdated = relationsupdated + "," + str(familyarray[i]["relation"])
                firstnamesupdated = firstnamesupdated + "," + str(familyarray[i]["firstname"])
                lastnamesupdated = lastnamesupdated + "," + str(familyarray[i]["lastname"])
                birthdaysupdated = birthdaysupdated + "," + str(familyarray[i]["birthday"])
                vitalstatusesupdated = vitalstatusesupdated + "," + str(familyarray[i]["vitalstatus"])

            relationsupdated = relationsupdated[1:]
            firstnamesupdated = firstnamesupdated[1:]
            lastnamesupdated = lastnamesupdated[1:]
            birthdaysupdated = birthdaysupdated[1:]
            vitalstatusesupdated = vitalstatusesupdated[1:]

            if not familyarray:
                familyput = requests.put('http://localhost:8000/api/family/'+str(loggedInUser["id"]), data = {"familylistowner_id":familylistowner_id,"relations":"","firstnames":"","lastnames":" ","birthdays":"","vitalstatuses":""}, headers={'Authorization': 'Bearer {}'.format(str(token))})
                if familyput.content:
                    return redirect('/')
            else:
                familyput = requests.put('http://localhost:8000/api/family/'+str(loggedInUser["id"]), data = {"familylistowner_id":familylistowner_id,"relations":relationsupdated,"firstnames":firstnamesupdated,"lastnames":lastnamesupdated,"birthdays":birthdaysupdated,"vitalstatuses":vitalstatusesupdated}, headers={'Authorization': 'Bearer {}'.format(str(token))})
                if familyput.content:
                    return redirect('/')
    else:
        return redirect('/signin')

def edituserdetails(request):
    token = str(request.session.get('accesstoken'))
    isLoggedIn = request.session.get('isLoggedIn')
    loggedInUser = request.session.get('loggedInUser')
    if isLoggedIn:
        if request.method == 'GET':
            return render(request, 'edituserdetails.html', {'user': loggedInUser, 'isLoggedIn':isLoggedIn})
        else:
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            email = request.POST['email']
            updateuser = requests.put('http://localhost:8000/api/users/'+str(loggedInUser["id"])+'/', data = {"id":loggedInUser["id"], "first_name":first_name, "last_name":last_name, "email": email}, headers={'Authorization': 'Bearer {}'.format(str(token))})
            if updateuser.content:
                updateduserjson = json.loads(updateuser.content)
                loggedInUser["first_name"] = first_name
                loggedInUser["last_name"] = last_name
                loggedInUser["email"] = email
                request.session['loggedInUser'] = updateduserjson
                family = requests.get('http://localhost:8000/api/family/'+str(loggedInUser["id"]), headers={'Authorization': 'Bearer {}'.format(token)})
                if family.content:
                    familyjson = json.loads(family.content)
                    familylistowner_id = familyjson["familylistowner_id"]
                    relations = familyjson["relations"].split(",")
                    firstnames = familyjson["firstnames"].split(",")
                    lastnames = familyjson["lastnames"].split(",")
                    birthdays = familyjson["birthdays"].split(",")
                    vitalstatuses = familyjson["vitalstatuses"].split(",")
                    familyarray = []
                    for i in range(len(relations)):
                        familyarray.append({"relation":relations[i], "firstname":firstnames[i], "lastname":lastnames[i], "birthday":birthdays[i], "vitalstatus":vitalstatuses[i]})
                    familyarray[0]["firstname"] = loggedInUser["first_name"]
                    familyarray[0]["lastname"] = loggedInUser["last_name"]

                    relationsupdated = ""
                    firstnamesupdated = ""
                    lastnamesupdated = ""
                    birthdaysupdated = ""
                    vitalstatusesupdated = ""

                    for i in range(len(familyarray)):
                        relationsupdated = relationsupdated + "," + str(familyarray[i]["relation"])
                        firstnamesupdated = firstnamesupdated + "," + str(familyarray[i]["firstname"])
                        lastnamesupdated = lastnamesupdated + "," + str(familyarray[i]["lastname"])
                        birthdaysupdated = birthdaysupdated + "," + str(familyarray[i]["birthday"])
                        vitalstatusesupdated = vitalstatusesupdated + "," + str(familyarray[i]["vitalstatus"])
                    relationsupdated = relationsupdated[1:]
                    firstnamesupdated = firstnamesupdated[1:]
                    lastnamesupdated = lastnamesupdated[1:]
                    birthdaysupdated = birthdaysupdated[1:]
                    vitalstatusesupdated = vitalstatusesupdated[1:]
                    familyput = requests.put('http://localhost:8000/api/family/'+str(loggedInUser["id"]), data = {"familylistowner_id":familylistowner_id,"relations":relationsupdated,"firstnames":firstnamesupdated,"lastnames":lastnamesupdated,"birthdays":birthdaysupdated,"vitalstatuses":vitalstatusesupdated}, headers={'Authorization': 'Bearer {}'.format(str(token))})
                    if familyput.content:
                        return redirect('/')
            return redirect('/')
    else:
        return redirect('/signin')

def changepass(request):
    token = str(request.session.get('accesstoken'))
    isLoggedIn = request.session.get('isLoggedIn')
    loggedInUser = request.session.get('loggedInUser')
    if isLoggedIn:
        if request.method == 'GET':
            return render(request, 'changepass.html', {'user':loggedInUser, 'isLoggedIn':isLoggedIn})
        else:
            password = request.POST['password']
            confirmpass = request.POST['confirmpass']
            if password != confirmpass:
                messages.info(request, 'Change password not successful.')
                return redirect('changepass')
            else:
                updatepass = requests.put('http://localhost:8000/api/users/'+str(loggedInUser["id"])+'/', data = {"id":loggedInUser["id"], "password":password}, headers={'Authorization': 'Bearer {}'.format(str(token))})
                if updatepass.content:
                    updateduserjson = json.loads(updatepass.content)
                    request.session['loggedInUser'] = updateduserjson
                    return redirect('/edituserdetails')
    else:
        return redirect('/signin')