from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('signup', views.signup, name='signup'),
    path('signin', views.signin, name='signin'),
    path('add', views.add, name='add'),
    path('edit<int:index>', views.edit, name='edit'),
    path('delete<int:index>', views.delete, name='delete'),
    path('signout', views.signout, name='signout'),
    path('edituserdetails', views.edituserdetails, name='edituserdetails'),
    path('changepass', views.changepass, name="changepass")
]